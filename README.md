<div align="center"><h1 align="center">AX</h3></div>
<div align="center"><h3 align="center">Windows 操作风格的后台管理系统</h3>
</div>
<p align="center">     
    <p align="center">
        <img src="https://img.shields.io/badge/AX-V1.0-green" alt="AX">
        <img src="https://img.shields.io/badge/AX-权限管理-red" alt="Gitee star">
        <img src="https://img.shields.io/badge/AX-可视化-blue" alt="Gitee fork">
    </p>
</p>


#### 关于预览

<img src="./public/desktop.webp" width="45%"><img src="./public/light.webp" width="45%">

<img src="./public/dark.webp" width="45%"><img src="./public/fullscreen.webp" width="45%">



#### 关于链接

<a src="https://www.yuque.com/u32997899/ax/kv2sq5i0nrz3831c?singleDoc" target="_blank">系统介绍</a>

<a src="http://150.158.14.110:8000/" target="_blank">在线预览</a>

系统介绍【搭建中】

系统规划【搭建中】



#### 关于项目

​	Windows上的操作交互非常丝滑，而Web上却总是磕磕碰碰，页面风格千奇百怪。弹窗套模态框，

模态框套抽屉。或许我们可以用一种新的风格去解决这些问题，那就是窗口。

​	AX已经封装好了大部分的交互逻辑，只需调用即可，比如新建一个窗口只需一个函数

```js
openWindow({
	comonent:你的组件,
	title:"窗口标题"
})
```

​	**框选，拖拽，右键**是每个页面的出厂设置，不需要一行代码

- ​	生成的代码附带全中文注释，告别阴阳怪气的洋文,任意字段都附带提示，不用担心用户看不懂表单信息了


- ​	代码分层：系统代码+业务代码；你甚至不需要了解系统代码是如何构成的。只需要在窗口写下业务逻辑


- ​	支持桌面图标风格切换，桌面壁纸切换，配置可视化，自带系统文件管理，多元组件


- ​	代码生成，极具生产力，页面操作风格高度统一。


- ​	自带记事本，图片，视频，浏览器四种内置插件

  <hr/>

​	目前主要由个人开发维护，从2024年/3月初开始构建

​	这是唯一仓库，你能找出的第二个，那就是从这里fork过去的。后端源自 <a hre="https://ruoyi.vip/" target="_blank">若依</a>，大改特改。



#### 关于FORK

- 个人开发者，学生，政府，微型企业完全免费
- 非商业用途的二开，请注明系统项目源地址  <a href=https://gitee.com/in-git/ax.git>AX</a>
- 更希望**AX**能成为一个商业化的开源项目，有资金链回报社区所有人，包含相关开发者，运营人员，UI设计等有贡献的人员
- 当前处于测试状态，项目基本成型，部分功能待测试



#### 多元组件

- 记事本

- 浏览器

- 路径选择器

- 图标选择器

- 视频

- 图片处理器

  

#### 关于特色

- [x] 生成的所有表格均支持**框选**，**右键功能**
- [x] 支持批量添加数据
- [x] 表头本地记忆，任何字段均能**配置富文本提示**信息，彻底解决字段盲区
- [x] 自带系统数据，如地区，备忘录，可快速选择
- [x] 支持服务器路径选择，可快速在此基础上开发业务
- [x] 完全支持服务器文件的增删改查



