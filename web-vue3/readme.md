<div align="center"><h1 align="center">AX</h3></div>
<div align="center"><h3 align="center">Windows 操作风格的后台管理系统</h3>
</div>
<p align="center">
   <img src="https://img.shields.io/badge/AX-V1.0-green" alt="AX">
   <img src="https://img.shields.io/badge/AX-权限管理-red" alt="Gitee star">
   <img src="https://img.shields.io/badge/AX-可视化-blue" alt="Gitee fork">
</p>
### 获取代码

```
git clone https://gitee.com/in-git/ax-web.git
```

### 安装

```bash
npm i
```

### 启动

```
npm run dev
```

### 分支说明

```

master:主分支
前后端分支要保持一致，否则会出现无法打开系统功能的情况，但不会影响系统运行
```



